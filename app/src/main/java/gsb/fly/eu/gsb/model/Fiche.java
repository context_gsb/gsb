package gsb.fly.eu.gsb.model;

import android.os.Parcel;
import android.os.Parcelable;

import gsb.fly.eu.gsb.R;

public class Fiche implements Parcelable {

    private String id;
    private String date;
    private String montant_valide;
    private String etat;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(date);
        dest.writeString(montant_valide);
        dest.writeString(etat);
    }

    public static final Parcelable.Creator<Fiche> CREATOR = new Parcelable.Creator<Fiche>() {

        public Fiche createFromParcel(Parcel in) {
            return new Fiche(in);
        }

        public Fiche[] newArray(int size) {
            return new Fiche[size];
        }
    };

    private Fiche(Parcel in) {
        id = in.readString();
        date = in.readString();
        montant_valide = in.readString();
        etat = in.readString();
    }

//    public Fiche(String id, String date, String montant_valide, String etat) {
//        this.id = id;
//        this.date = date;
//        this.montant_valide = montant_valide;
//        this.etat = etat;
//    }

    public String getId() {
        return id;
    }

    public void setId(String fiche_id) {
        this.id = fiche_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMontant_valide() {
        return montant_valide;
    }

    public void setMontant_valide(String montant_valide) {
        this.montant_valide = montant_valide;
    }

    public String getEtat() {
        String strEtat;
        switch (etat){
            case "1": strEtat =  "Crée";
                break;
            case "2": strEtat =  "Cloturée";
                break;
            case "3": strEtat =  "Validée";
                break;
            case "4": strEtat =  "Refusée";
                break;
            case "5": strEtat =  "Mise en paiement";
                break;
            case "6": strEtat =  "Remboursée";
                break;
            case "7": strEtat =  "En Traitement";
                break;
            default: strEtat = "Undefined";
                break;
        }

        return strEtat;
    }

    public int getEtatColor(){
        int color;
        switch (etat){
            case "1": color =  R.color.cree;
                break;
            case "2": color =  R.color.cloture;
                break;
            case "3": color =  R.color.valide;
                break;
            case "4": color =  R.color.refuse;
                break;
            case "5": color =  R.color.paiement;
                break;
            case "6": color =  R.color.rembourse;
                break;
            case "7": color =  R.color.traitement;
                break;
            default: color =  R.color.refuse;
                break;
        }

        return color;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }
}
