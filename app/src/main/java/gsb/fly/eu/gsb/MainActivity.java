package gsb.fly.eu.gsb;

import gsb.fly.eu.gsb.adapter.FraisAdapter;
import gsb.fly.eu.gsb.model.Fiche;
import gsb.fly.eu.gsb.model.Frais;
import gsb.fly.eu.gsb.model.User;
import gsb.fly.eu.gsb.remote.ApiService;
import gsb.fly.eu.gsb.remote.ApiUtils;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Fiche> ficheArrayList = new ArrayList<>();
    private ArrayList<Frais> fraisArrayList = new ArrayList<>();
    private FraisAdapter fraisAdapter;
    ApiService apiService;
    TextView txtUsername;
    Button btnAncienneFiche;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Find View
        recyclerView = findViewById(R.id.recyclerView);
        txtUsername = findViewById(R.id.txtUsername);

        btnAncienneFiche = findViewById(R.id.btnAncienneFiche);

        apiService = ApiUtils.getApiService();

        // User data
        Intent intent = getIntent();
        user = intent.getParcelableExtra("user");

        // Set date
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        String name_id = user.getName();
        txtUsername.setText(name_id);

        Call<ArrayList<Fiche>> call = apiService.getFiche(user.getToken(), user.getUser_id());
        call.enqueue(new Callback<ArrayList<Fiche>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<Fiche>> call, @NonNull Response<ArrayList<Fiche>> response) {
                Log.i("log: ", response.toString());
                if(response.isSuccessful()){
                    ficheArrayList = response.body();
                    if(ficheArrayList != null && !ficheArrayList.isEmpty()){
                        Log.i("log: ", ficheArrayList.toString());
                        Collections.reverse(ficheArrayList);
                        setRecyclerView();
                    }
                    else{
                        Log.i("log: ", "Pas de fiche");
                    }
                }
                else{
                    Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    Log.i("log", response.toString());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Fiche>> call, @NonNull Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.i("log", t.getMessage());
            }
        });

        btnAncienneFiche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListeFicheActivity.class);
                intent.putExtra("user", user);
                startActivity(intent);
            }
        });
    }

    private void setRecyclerView(){
        Log.i("Log: ", "set recycler view");
        Fiche fiche = ficheArrayList.get(0);
        Call<ArrayList<Frais>> call = apiService.getDetailFiche(user.getToken(), fiche.getId());
        call.enqueue(new Callback<ArrayList<Frais>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<Frais>> call, @NonNull Response<ArrayList<Frais>> response) {
                Log.i("Response: ", response.toString());
                if(response.isSuccessful()){
                    fraisArrayList = response.body();
                    if(fraisArrayList != null && !fraisArrayList.isEmpty()){
                        Log.i("Pas vide: ", "Main activity liste frais pas vide");
                    }
                    else{
                        Log.i("Log: ", "Pas de Frais");
                        Frais frais = new Frais("", "Pas de frais", ".", "","8");
                        fraisArrayList.add(frais);
                    }
                    fraisAdapter = new FraisAdapter(fraisArrayList);
                    recyclerView.setAdapter(fraisAdapter);
                }
                else{
                    Log.i("Log", "Error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Frais>> call, @NonNull Throwable t) {
                Log.i("Failure: ", t.getMessage());
            }
        });
    }
}
