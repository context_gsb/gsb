package gsb.fly.eu.gsb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import gsb.fly.eu.gsb.adapter.FraisAdapter;
import gsb.fly.eu.gsb.model.Fiche;
import gsb.fly.eu.gsb.model.Frais;
import gsb.fly.eu.gsb.model.User;
import gsb.fly.eu.gsb.remote.ApiService;

import gsb.fly.eu.gsb.remote.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailFicheActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Frais> fraisArrayList = new ArrayList<>();
    private FraisAdapter fraisAdapter;
    ApiService apiService;
    TextView detailFiche;
    TextView dateFiche;
    User user;
    Fiche fiche;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_fiche);

        // Find View
        recyclerView = findViewById(R.id.recyclerView);
        detailFiche = findViewById(R.id.detailFiche);
        dateFiche = findViewById(R.id.dateFiche);
        apiService = ApiUtils.getApiService();

        // Fiche Data
        Intent intent = getIntent();
        user = (User) intent.getParcelableExtra("user");
        fiche = (Fiche) intent.getParcelableExtra("fiche");

        // Set data
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        dateFiche.setText(fiche.getDate());

        Log.i("User: ", user.getName());
        Log.i("Fiche: ", fiche.getId() + " " + fiche.getDate());

        Call<ArrayList<Frais>> call = apiService.getDetailFiche(user.getToken(), fiche.getId());
        call.enqueue(new Callback<ArrayList<Frais>>() {
            @Override
            public void onResponse(Call<ArrayList<Frais>> call, Response<ArrayList<Frais>> response) {
                Log.i("Response: ", response.toString());
                if(response.isSuccessful()){
                    fraisArrayList = response.body();
                    if(fraisArrayList != null && !fraisArrayList.isEmpty()){
                        Log.i("Pas vide: ", "Detail fiche activity liste frais pas vide");
                    }
                    else{
                        Toast.makeText(DetailFicheActivity.this, "Pas de Frais", Toast.LENGTH_SHORT).show();
                        Log.i("Log: ", "Pas de Frais");
                        Frais frais = new Frais("", "Pas de frais", ".", "","8");
                        fraisArrayList.add(frais);
                    }
                    fraisAdapter = new FraisAdapter(fraisArrayList);
                    recyclerView.setAdapter(fraisAdapter);
                }
                else{
                    Toast.makeText(DetailFicheActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Frais>> call, Throwable t) {
                Log.i("Failure: ", t.getMessage());
                Toast.makeText(DetailFicheActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
