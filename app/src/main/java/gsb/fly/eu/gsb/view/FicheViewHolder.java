package gsb.fly.eu.gsb.view;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import gsb.fly.eu.gsb.R;
import gsb.fly.eu.gsb.adapter.FicheAdapter;
import gsb.fly.eu.gsb.model.Fiche;

public class FicheViewHolder extends RecyclerView.ViewHolder {

    private TextView idFiche;
    private TextView dateFiche;
    private TextView montantValide;
    private TextView etatFiche;

    public FicheViewHolder(View itemView, final FicheAdapter.OnItemClickListener listener){
        super(itemView);

        idFiche = itemView.findViewById(R.id.idFiche);
        dateFiche = itemView.findViewById(R.id.dateFiche);
        montantValide = itemView.findViewById(R.id.montantValide);
        etatFiche = itemView.findViewById(R.id.etatFiche);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    int position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION){
                        listener.onItemClick(position);
                    }
                }
            }
        });
    }

    public void bind(Fiche fiche, View itemView){
        idFiche.setText(fiche.getId());
        dateFiche.setText(fiche.getDate());
        montantValide.setText(fiche.getMontant_valide());
        etatFiche.setText(fiche.getEtat());
        etatFiche.setTextColor(ContextCompat.getColor(itemView.getContext(), fiche.getEtatColor()));
    }
}
