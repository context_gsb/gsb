package gsb.fly.eu.gsb.view;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import gsb.fly.eu.gsb.R;
import gsb.fly.eu.gsb.model.Frais;

public class FraisViewHolder extends RecyclerView.ViewHolder {

    private TextView libelle;
    private TextView montant;
    private TextView date_frais;
    private TextView etat;

    public FraisViewHolder(View itemView) {
        super(itemView);

        libelle = itemView.findViewById(R.id.libelleFrais);
        montant = itemView.findViewById(R.id.montantFrais);
        date_frais = itemView.findViewById(R.id.dateFrais);
        etat = itemView.findViewById(R.id.etatFrais);
    }

    public void bind(Frais frais, View itemView){
        libelle.setText(frais.getLibelle());
        montant.setText(frais.getMontant());
        date_frais.setText(frais.getDate());
        etat.setText(frais.getEtat());
        etat.setTextColor(ContextCompat.getColor(itemView.getContext(), frais.getEtatColor()));
    }
}
