package gsb.fly.eu.gsb.adapter;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import gsb.fly.eu.gsb.R;
import gsb.fly.eu.gsb.model.Frais;
import gsb.fly.eu.gsb.view.FraisViewHolder;

public class FraisAdapter extends RecyclerView.Adapter<FraisViewHolder>{

    private ArrayList<Frais> fraisArrayList;

    public FraisAdapter(ArrayList<Frais> fraisArrayList){
        this.fraisArrayList = fraisArrayList;
    }

    @NonNull
    @Override
    public FraisViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.frais_ligne, parent, false);
        return new FraisViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FraisViewHolder holder, int position) {
        Frais frais = fraisArrayList.get(position);
        View itemView = holder.itemView;
        holder.bind(frais, itemView);
    }

    @Override
    public int getItemCount() {
        return fraisArrayList.size();
    }
}
