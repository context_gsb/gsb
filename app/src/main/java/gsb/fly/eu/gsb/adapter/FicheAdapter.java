package gsb.fly.eu.gsb.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import gsb.fly.eu.gsb.R;
import gsb.fly.eu.gsb.model.Fiche;
import gsb.fly.eu.gsb.view.FicheViewHolder;

public class FicheAdapter extends RecyclerView.Adapter<FicheViewHolder> {

    private ArrayList<Fiche> list_fiche;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public FicheAdapter(ArrayList<Fiche> list_fiche){
        this.list_fiche = list_fiche;
    }

    @NonNull
    @Override
    public FicheViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fiche_ligne, viewGroup, false);
        return new FicheViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull FicheViewHolder ficheViewHolder, int position){
        Fiche fiche = list_fiche.get(position);
        View itemView = ficheViewHolder.itemView;
        ficheViewHolder.bind(fiche, itemView);
    }

    @Override
    public int getItemCount(){
        return list_fiche.size();
    }

}
