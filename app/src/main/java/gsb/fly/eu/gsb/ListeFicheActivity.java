package gsb.fly.eu.gsb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import gsb.fly.eu.gsb.adapter.FicheAdapter;
import gsb.fly.eu.gsb.model.Fiche;
import gsb.fly.eu.gsb.model.User;
import gsb.fly.eu.gsb.remote.ApiService;
import gsb.fly.eu.gsb.remote.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListeFicheActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Fiche> ficheArrayList = new ArrayList<>();
    private FicheAdapter ficheAdapter;
    private User user;
    ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_fiche);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        apiService = ApiUtils.getApiService();

        Intent intent = getIntent();
        user = (User) intent.getParcelableExtra("user");

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Call<ArrayList<Fiche>> call = apiService.getFiche(user.getToken(), user.getUser_id());
        call.enqueue(new Callback<ArrayList<Fiche>>() {
            @Override
            public void onResponse(Call<ArrayList<Fiche>> call, Response<ArrayList<Fiche>> response) {
                Log.i("log: ", response.toString());
                if(response.isSuccessful()){
                    ficheArrayList = response.body();

                    if(ficheArrayList != null && !ficheArrayList.isEmpty()){
                        Log.i("log: ", ficheArrayList.toString());
                        Collections.reverse(ficheArrayList);
                        ficheAdapter = new FicheAdapter(ficheArrayList);
                        setRecyclerView(ficheAdapter);
                    }
                    else{
                        Toast.makeText(ListeFicheActivity.this, "Pas de Fiche", Toast.LENGTH_SHORT).show();
                        Log.i("log: ", "Pas de fiche");
                    }
                }
                else{
                    Toast.makeText(ListeFicheActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    Log.i("log", response.toString());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Fiche>> call, Throwable t) {
                Toast.makeText(ListeFicheActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.i("log", t.getMessage());
            }
        });
    }

    private void setRecyclerView(FicheAdapter ficheAdapter){
        recyclerView.setAdapter(ficheAdapter);
        ficheAdapter.setOnItemClickListener(new FicheAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Fiche fiche = ficheArrayList.get(position);
                Intent intent = new Intent(ListeFicheActivity.this, DetailFicheActivity.class);
                intent.putExtra("user", user);
                intent.putExtra("fiche", fiche);
                startActivity(intent);
            }
        });
    }
}
