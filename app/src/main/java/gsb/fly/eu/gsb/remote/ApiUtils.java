package gsb.fly.eu.gsb.remote;

public class ApiUtils {

    public static final String BASE_URL = "http://flygsb.ddns.net";

    public static ApiService getApiService(){
        return RetrofitClient.getClient(BASE_URL).create(ApiService.class);
    }
}
