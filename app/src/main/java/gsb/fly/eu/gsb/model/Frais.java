package gsb.fly.eu.gsb.model;

import com.google.gson.annotations.SerializedName;

import gsb.fly.eu.gsb.R;

public class Frais {

    @SerializedName("type_f")
    private String type_f;
    @SerializedName("libelle")
    private String libelle;
    @SerializedName("montant")
    private String montant;
    @SerializedName("date")
    private String date;
    @SerializedName("etat")
    private String etat;

    public Frais(String type_f, String libelle, String montant, String date, String etat) {
        this.type_f = type_f;
        this.libelle = libelle;
        this.montant = montant;
        this.date = date;
        this.etat = etat;
    }

    public String getType_f() {
        return type_f;
    }

    public void setType_f(String type_f) {
        this.type_f = type_f;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getMontant() {
        return montant;
    }

    public void setMontant(String montant) {
        this.montant = montant;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }
    public String getEtat() {
        String strEtat;
        switch (etat){
            case "1": strEtat =  "Crée";
                break;
            case "2": strEtat =  "Cloturée";
                break;
            case "3": strEtat =  "Validée";
                break;
            case "4": strEtat =  "Refusée";
                break;
            case "5": strEtat =  "Mise en paiement";
                break;
            case "6": strEtat =  "Remboursée";
                break;
            case "7": strEtat =  "En Traitement";
                break;
            case "8": strEtat =  "";
                break;
            default: strEtat = "Undefined";
                break;
        }

        return strEtat;
    }

    public int getEtatColor(){
        int color;
        switch (etat){
            case "1": color = R.color.cree;
                break;
            case "2": color =  R.color.cloture;
                break;
            case "3": color =  R.color.valide;
                break;
            case "4": color =  R.color.refuse;
                break;
            case "5": color =  R.color.paiement;
                break;
            case "6": color =  R.color.rembourse;
                break;
            case "7": color =  R.color.traitement;
                break;
            case "8": color = R.color.refuse;
                break;
            default: color =  R.color.refuse;
                break;
        }

        return color;
    }
}
