package gsb.fly.eu.gsb.remote;

import java.util.ArrayList;

import gsb.fly.eu.gsb.model.Frais;
import gsb.fly.eu.gsb.model.Fiche;
import gsb.fly.eu.gsb.model.User;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {

    @POST("api-token-auth/")
    @FormUrlEncoded
    Call<User> getToken(@Field("username") String first, @Field("password") String last);

    @GET("api-rest/detail_fiche_frais/{fiche_id}/")
    Call<ArrayList<Frais>> getDetailFiche(@Header("Authorization") String authorization, @Path("fiche_id") String fiche_id);

    @GET("api-rest/user_fiche_frais/{user_id}/")
    Call<ArrayList<Fiche>> getFiche(@Header("Authorization") String authorization, @Path("user_id") String user_id);
}
